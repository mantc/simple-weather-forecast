Simple script using VueJS to display data from api.weather.gov.
See the help button for suggested use.
To understand how this works, refer to https://www.weather.gov/documentation/services-web-api for
overview and examples.  

Basically, a latitude and longitude coordinate pair must be entered.
This can be easily copied to the clipboard from Google Maps and pasted into the field.
This is used to first determine the nearest station with weather to report.
The weather data is then retrieved from this station.