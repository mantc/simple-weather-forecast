
const app = new Vue({

    el: '#weather',

    data: {
        latlong: '',
        office: '',
        gridX: '',
        gridY: '',
        when: '',
        temp: '',
        details: '',
    },

   methods: {
        findTheStation: function() {
            this.when = 'WORKING.....'  // sometimes this takes a while to complete
            this.temp = ''
            this.details = ''
            // Google Maps coordinate pairs are separated by a space which must be removed for constructing API endpoint
            let removedSpacesString = this.latlong.split(" ").join("");
 
            let url = 'https://api.weather.gov/points/'+removedSpacesString
            console.log('URL to find station ID:\n'+url)
            fetch(url)

            .then((response) => {
                return response.json()
            })
    
            .then((data) => {
                console.log('Station ID data:\n', data)
                if (data.status == 404) {
                    alert('Location Not Found\nSelect Location in US\nand try again.')
                    this.when = ''
                }
                else {
                    this.office = data.properties.gridId
                    this.gridX = data.properties.gridX
                    this.gridY = data.properties.gridY
                    this.getForecastData()                               
                }
            })
    
            .catch((err) => {
                console.log(err);     
            })
        },
        getForecastData: function() {
            let url = 'https://api.weather.gov/gridpoints/'+this.office+'/'+this.gridX+','+this.gridY+'/forecast'
            console.log('URL to get forecast data:\n'+url)
            fetch(url)

            .then((response) => {
                return response.json()
            })
    
            .then((data) => { 
                console.log('Station weather data:\n', data)
                if (data.status == 500) {  // this happens occasionally
                    alert('An unexpected problem has occured with the request\nPlease try again.')
                }
                else {
                    this.when = data.properties.periods[0].name
                    this.temp = data.properties.periods[0].temperature
                    this.details = data.properties.periods[0].detailedForecast
                }
            })
    
            .catch((err) => {
                console.log(err)     
            })
        },
        showHelp: function() {
            alert('In Google Maps, hover over desired location in US and right-click.\nClick on coordinates displayed to copy them to clipboard.\nPaste coordinates into field above and click Get Forecast.')
        }

    }
})
